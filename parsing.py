import re
import requests

REGEX_TOTAL_SESSIONS = r'((\S+) +(\S+) +(\S+)) +(\S+) "webbridge":  INFO : \[DEBUGGING\] Stats (\d+), c:(\d+), d:(\d+)'


def create_database(database, host="localhost", port=8086, scheme='http'):
    query = "CREATE DATABASE %s" % database
    api_url = "%s://%s:%s/query?q=%s" % (scheme, host, port, query)
    if requests.get(api_url).status_code != 200:
        print("Failed to create database %s" % database)


def influxdb_push(data, host="localhost", port=8086,
                  scheme='http', database='test'):
    api_url = "%s://%s:%s/write?db=%s" % (scheme, host, port, database)
    res = requests.post(api_url, data=data)
    if res.status_code != 204:
        print("Failed to add %s to %s" % (data, api_url))
        print(res.content)


def read_data(path):
    data = []
    try:
        with open(path, "r", encoding="utf8") as f:
            data = f.readlines()
    except IOError as e:
        print(e)
    return data


def parse(data):
    for line in data:
        matchs = re.search(REGEX_TOTAL_SESSIONS, line)
        if matchs is not None:
            print(matchs.groups())


if __name__ == '__main__':
    data = read_data("./samples/visio.log")
    parse(data)
