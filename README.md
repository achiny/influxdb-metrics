# influxdb-metrics

Pour réaliser cette mise en application, vous pouvez soit:

* Utiliser le fichier `docker-compose.yml` directement depuis votre machine
* Utiliser la machine `sesi-elk-web.inria.fr` et `qlf-influxdb.inria.fr` depuis `qlf-sesi-sable.inria.fr`

> L'utilisation du fichier `docker-compose.yml` nécessite l'installation de docker et docker-compose

* https://docs.docker.com/get-docker/
* https://docs.docker.com/compose/install/

On peut aussi utiliser `podman` et `podman-compose`

```
sudo dnf install -y podman
pip3 install --user -U podman-compose

podman-compose up
```

Connexion à `qlf-sesi-sable.inria.fr`

```bash
ssh -J achiny@sesi-ssh.inria.fr,achiny@sesi-bd.inria.fr achiny@qlf-sesi-sable.inria.fr
```

Pour cloner le dépôt utiliser la commande suivante :

```
git clone https://gitlab.inria.fr/achiny/influxdb-metrics.git
```

Ou récupérer le script directement

```bash
mkdir samples && wget -O samples/vm_list.json https://gitlab.inria.fr/achiny/influxdb-metrics/-/raw/master/samples/vm_list.json
wget https://gitlab.inria.fr/achiny/influxdb-metrics/-/raw/master/aggregations.py
```

## Objectifs

Envoyer les données transformées dans InfluxDB

Création d'un tableau de bord sur Grafana

## Aggrégation de données JSON

Les données : `samples/vm_list.json`

Format des données d'une machine :

```json
    {
	"name": "vm1",
	"state": "on",
	"site": "Paris"
    }
```

Grouper les machines par `site` puis par `state`

J'ai ajouté une solution possible dans le fichier `aggregations.py`

* Le script crée une base `test`
* Lis le fichier `samples/vm_list.json`
* Transforme les données et les ajoute dans InfluxDB

Pour lancer le script :

```
python3 aggregations.py
```

Une fois les données **ajoutées**

Connectez vous à Grafana en utilisant les identifiants `admin:admin` si vous utilisez Docker sinon vos identifiants **Inria**

On peut ajouter une nouvelle **datasource** dans Grafana

![](./images/add_datasource.gif)

En utilisant l'environement Docker :

![](./images/datasource.png)

Puis configurer notre première **visualisation**

Ici la répartition des machines avec un état *off* groupé par **site**

![](./images/first_visualisation.gif)

Calcul du pourcentage de machines arretées par site

* Pour calculer le total de machine (`last(on) + last(off)`)

![](./images/visualisation_ratio.gif)

Quelques idées de visualisation :
* création d'une variable `Site` pour avoir des graphiques relatifs à un seul site (cf: [doc](https://grafana.com/docs/grafana/latest/features/datasources/influxdb/#query-variable))
* le ratio on/off pour un site spécifique
* l'évolution d'une nombre de machines arretées par site

## Parsing de logs

Les données : `samples/visio.log`

Format d'une ligne de log :

`[[Mon]] [[Day]] [[HH:MM:SS]] [[Hostname]] "webbridge":  INFO : [DEBUGGING] Stats [[sessions]], c:[[socket_created]], d:[[socket_destroyed]]`

Pour le parsing d'une ligne j'ai ajouté l'expression régulière correspondante.

`REGEX_TOTAL_SESSIONS = r'((\S+) +(\S+) +(\S+)) +(\S+) "webbridge":  INFO : \[DEBUGGING\] Stats (\d+), c:(\d+), d:(\d+)'`

L'objectif est de récupérer la date pour la convertir en timestamp (ms).

Le nom d'hôte puis le nombre de session et les statistiques relative aux sockets.

Quelques idées de visualisation :
* nombre maximum de session sur 24 heures
* le nombre de sockets actives (`socket_created - socket_destroyed`) par hôte
* création d'une variable `Hôte` pour avoir des graphiques relatifs à un seul hôte

## Références

* https://grafana.com/docs/grafana/latest/features/datasources/influxdb/
* https://docs.influxdata.com/influxdb/v1.8/guides/write_data/
* https://docs.influxdata.com/influxdb/v1.8/guides/calculate_percentages/
* https://requests.readthedocs.io/en/master/
* https://docs.python.org/fr/3/library/itertools.html
* https://docs.python.org/3/library/re.html
