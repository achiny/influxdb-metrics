import os
import json
import requests
import datetime
from itertools import groupby


def timestamp_range(step=0, interval=datetime.timedelta(days=1)):
    now = datetime.datetime.now()
    for i in range(step):
        yield int((now - interval * i).timestamp()*1000)

def get_site(vm):
    return vm.get("site")


def get_state(vm):
    return vm.get("state")


def read_sample(path):
    data = {}

    try:
        with open(path, 'r', encoding='utf8') as f:
            data = json.load(f)
    except IOError as e:
        print(e)
    return data


def groupby_site(data):
    for site, vms in groupby(data, get_site):
        yield site, vms


def groupby_state(data):
    for state, vms in groupby(data, get_state):
        yield state, vms


def compute_totals(data):
    """ For each sites, count the number of vm on/off
    """
    totals = {}

    for site, groups in groupby_site(data):
        if totals.get(site) is None:
            totals[site] = {
                "on": 0,
                "off": 0,
            }
        for state, vms in groupby_state(groups):
            totals[site][state] += len(list(vms))
    return totals


def create_database(database, host="localhost", port=8086, scheme='http'):
    query = "CREATE DATABASE %s" % database
    api_url = "%s://%s:%s/query?q=%s" % (scheme, host, port, query)
    if requests.get(api_url).status_code != 200:
        print("Failed to create database %s" % database)
    else:
        print("Create database %s" % (database))


def influxdb_push(data, host="localhost", port=8086,
                  scheme='http', database='test', precision='ns'):
    api_url = "%s://%s:%s/write?db=%s&precision=%s" % (scheme, host, port, database, precision)
    res = requests.post(api_url, data=data)
    if res.status_code != 204:
        print("Failed to add %s" % (data, api_url))
        print(res.content)
    else:
        print("Add %s" % data)


def push_totals(measurement, totals, timestamp=None, database='test', host='localhost'):
    for site, totals in totals.items():
        template = "%s,site=%s on=%s,off=%s"
        if timestamp is not None:
            template += " %s" % timestamp
        point = template % (measurement, site, totals['on'], totals['off'])
        influxdb_push(point, host=host, database=database, precision='ms')


def main():
    sample = "./samples/vm_list.json"

    if os.getenv("HOSTNAME") != "qlf-sesi-sable.inria.fr":
        database = "test"
        host = "localhost"
    else:
        database = "test_" + os.getenv("USER")
        host = "qlf-influxdb.inria.fr"

    # Read the data sample
    data = read_sample(sample)
    # Aggregations + sum
    totals = compute_totals(data)
    # Create the database
    create_database(database, host=host)
    # Push data to InfluxDB
    for timestamp in timestamp_range(step=7):
        push_totals("vm_totals", totals, timestamp=timestamp,
                    database=database, host=host)


if __name__ == '__main__':
    main()
